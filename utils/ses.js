const AWS = require("aws-sdk");
const env = require("dotenv");

env.config();
const errorFunction = require('./errorFunction');

const awsConfig = {
  accessKeyId: process.env.AccessKey,
  secretAccessKey: process.env.SecretKey,
  region: process.env.region
};

const SES = new AWS.SES(awsConfig);

const sendEmail = async (email, password) => {
  const aws_email = process.env.FROM_EMAIL
  try {
    var params = {
      Source: aws_email,
      Destination: {
        ToAddresses: [email],
      },
      Message: {
        Subject: {
          Charset: "UTF-8",
          Data: 'your new Password'
        },
        Body: {
          Html: {
            Charset: "UTF-8",
            Data: `<h4>your new Password id : ${password}</h4>`
          },
        },
      },
    };
    let sendPromise = SES.sendEmail(params).promise();
    sendPromise.then((data) => {
      if (data.MessageId) {
        return data.MessageId;
      }
    }).catch((err) => {
      console.log(err)
      return false
    })
  } catch (error) {
    console.log(error)
  }
};

module.exports = sendEmail