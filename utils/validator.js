const joi = require("joi");

/// ADMIN VALIDATOR ///

const adminLogin = joi.object({
  email: joi.string().email().trim(true).required(),
  password: joi.string().min(3).max(15).trim(true).required(),
});

const adminSignUp = joi.object({
  userName: joi.string().alphanum().min(3).max(25).trim(true).required(),
  email: joi.string().email().trim(true).required(),
  password: joi.string().min(3).max(15).trim(true).required(),
  mobileNumber: joi.string().length(10).pattern(/[6-9]{1}[0-9]{9}/).optional(),
});

const adminCreateCompany = joi.object({
  companyName: joi.string().min(3).max(25).trim(true).required(),
});

/// USER VALIDATOR ///

const userSignUp = joi.object({
  title: joi.string().trim(true).required(),
  firstName: joi.string().alphanum().min(3).max(25).trim(true).required(),
  email: joi.string().email().trim(true).required(),
  password: joi.string().min(3).max(15).trim(true).required(),
  mobileNumber: joi.string().length(10).pattern(/[6-9]{1}[0-9]{9}/).optional(),
  country: joi.string().optional(),
  address: joi.string().optional(),
  is_active: joi.boolean().default(true),
});

const emailValidator = joi.object({
  email: joi.string().email().trim(true).required(),
});

const userIdValidator = joi.object({
  userId: joi.string().trim(true).required(),
});

const contactValidator = joi.object({
  userId: joi.string().trim(true).required(),
  subject: joi.string().trim(true).required(),
  content: joi.string().trim(true).required()
});

const adminCreateGroup = joi.object({
  name: joi.string().trim(true).required(),
  symbol: joi.string().trim(true).required()
});

module.exports = { contactValidator,emailValidator, adminLogin, adminSignUp, adminCreateCompany, userSignUp, userIdValidator,adminCreateGroup };