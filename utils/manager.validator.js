const errorFunction = require('./errorFunction');
const {adminLogin, adminSignUp, adminCreateCompany} = require('./validator');

const login = async (req, res, next) => {
  const payload = {
    email: req.body.email,
    password: req.body.password,
  };

  const { error } = adminLogin.validate(payload);
  if (error) {
    res.status(406);
    return res.json(
      errorFunction(true, `Error in Admin Login Data : ${error.message}`)
    );
  } else {
    next();
  }
};

const signUp = async (req, res, next) => {
  const payload = {
    userName: req.body.userName,
    email: req.body.email,
    password: req.body.password,
    mobileNumber: req.body.mobileNumber,
  };

  const { error } = adminSignUp.validate(payload);
  if (error) {
    res.status(406);
    return res.json(
      errorFunction(true, `Error in Sign Up Admin Data : ${error.message}`)
    );
  } else {
    next();
  }
};

const createCompany = async (req, res, next) => {
  const payload = {
    companyName: req.body.companyName,
  };

  const { error } = adminCreateCompany.validate(payload);
  if (error) {
    res.status(406);
    return res.json(
      errorFunction(true, `Error in Create Company Data : ${error.message}`)
    );
  } else {
    next();
  }
};
const createGroup = async (req, res, next) => {
  const payload = {
    name: req.body.name,
    symbol: req.body.symbol,
  };

  const { error } =adminCreateGroup.validate(payload);
  if (error) {
    res.status(406);
    return res.json(
      errorFunction(true, `Error in Create Group Data : ${error.message}`)
    );
  } else {
    next();
  }
};

module.exports = { signUp, login, createCompany, createGroup };