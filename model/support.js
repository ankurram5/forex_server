const mongoose = require('mongoose');
const supportSchema = new mongoose.Schema({
  userId: {
    required: true,
    type: String
  },
  subject: {
    required: true,
    type: String
  },
  content: {
    required: true,
    type: String
  },
  file: {
    data: Buffer,
    contentType: String
  }
})

module.exports = mongoose.model('Support', supportSchema)