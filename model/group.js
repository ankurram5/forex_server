const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
var SchemaTypes = mongoose.Schema.Types;

const groupSchema = new mongoose.Schema({
  name: {
    required: true,
    type: String
  },
  symbols: [
    {
      symbol: String,
    }
  ],
  // members: [
  //   {
  //     userId: String

  //   }
  // ],

  // commission:
  // {
  //   required: true,
  //   type: mongoose.Schema.Types.Decimal128
  // },

  // timeintervalfrom:
  // {
  //   required: true,
  //   type: String
  // },

  // timeintervalto:
  // {
  //   required: true,
  //   type: String
  // },

  // spread:
  // {
  //   required: true,
  //   type: String
  // },

  status:
  {
    type: Boolean
  },

  createdAt: {
    type: String
  },

  updatedAt: {
    type: String
  }
});


module.exports = mongoose.model('Group', groupSchema)