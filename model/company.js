const mongoose = require('mongoose');
const companySchema = new mongoose.Schema({
  name: {
    required: true,
    type: String
  },
  logo: {
    required: false,
    type: String
  },
  status: {
    required: true,
    type: Boolean
  },
  createdAt: {
    type: String
  },
  updatedAt: {
    type: String
  }
})

module.exports = mongoose.model('Company', companySchema)
