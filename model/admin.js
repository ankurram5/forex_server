const mongoose = require('mongoose');
const adminSchema = new mongoose.Schema({
  userName: {
    required: true,
    type: String,
    unique: true,
    trim: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
    trim: true
  },
  roles: {
    type: [String]
  },
  access: {
    givingbonus: Boolean,
    changingleverage: Boolean,
    changinggroup: Boolean,
    changingspreadcalculation: Boolean,
    changingpasswordofclient: Boolean,
    seeclientrecords: Boolean,
    changinggroupswapgrouporsymbol: Boolean,
    companygroup: Boolean,
    clientinformations: Boolean,
    addremovedeposit: Boolean
  },
  password: {
    type: String,
    required: true,
    trim: true,
  },
  mobileNumber: {
    type: String,
    maxlength: 10,
    required: true,
    unique: true
  },
  createdAt: {
    type: String
  },
  updatedAt: {
    type: String
  }
})

module.exports = mongoose.model('Admin', adminSchema)


/*
const mongoose = require('mongoose');
const adminSchema = new mongoose.Schema({
  userName: {
    required: true,
    type: String,
    unique: true,
    trim: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
    trim: true
  },
  password: {
    type: String,
    required: true,
    trim: true,
  },
  mobileNumber: {
    type: String,
    maxlength: 10,
    required: true,
    unique: true
  },
  createdAt: { 
    type: String
  },
  updatedAt: {
    type: String
  }
})

module.exports = mongoose.model('Admin', adminSchema)
*/