const User = require('../model/user');
const Support = require('../model/support');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const AWS = require("aws-sdk");
const moment = require('moment');
const Worker = require("worker-thread");

const errorFunction = require('../utils/errorFunction');
const securePassword = require('../utils/securePassword')
const services = require('../services/commonServices')
const sendEmailServices = require('../utils/ses')

exports.signupUser = async (req, res, next) => {
  try {
    const existingUser = await User.findOne({ email: req.body.email, }).lean(true);
    if (existingUser) {
      res.status(403);
      return res.json(errorFunction(true, "User Already Exists"));
    } else {
      const hashedPassword = await securePassword(req.body.password);
      const newUser = await User.create({
        title: req.body.title,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hashedPassword,
        mobileNumber: req.body.mobileNumber,
        country: req.body.country,
        state: req.body.state,
        address: req.body.address,
        postCode: req.body.postCode,
        is_active: false,
        status: true,
        createdAt: moment().format("ll"),
        updatedAt: new Date().getTime()
      })
      if (newUser) {
        let id = { userId: newUser._id }
        res.status(201);
        return res.json(errorFunction(false, "User Created", id));
      } else {
        res.status(403);
        return res.json(errorFunction(true, "Error Creating User"));
      }
    }
  } catch (error) {
    res.status(400);
    console.log(error);
    return res.json(errorFunction(true, "Error Adding user"));
  }
};

exports.loginUser = async (req, res, next) => {
  const { email, password } = req.body
  const user = await User.findOne({ email })
  if (user && (await bcrypt.compare(password, user.password))) {
    let payload = {
      "userId": user._id,
      "email": user.email
    }
    const token = await jwt.sign({ id: user._id }, process.env.JWT_SECRET_KEY, {
      expiresIn: process.env.JWT_TOKEN_EXPIRED
    })
    let updateToken = await User.updateOne({ _id: user._id }, { $set: { authToken: token, is_active: true } });
    res.status(200);
    return res.json(errorFunction(false, "Login Successfully", user));
  } else {
    res.status(403);
    return res.json(errorFunction(true, "Invalid credentials"));
  }
}

exports.forgotPassword = async (req, res, next) => {
  const { email } = req.body
  try {
    let user = await services.userDetails(email);
    if (!user) {
      res.status(403);
      return res.json(errorFunction(true, "Unknown user"));
    }
    const newPassword = await services.generatePassword();
    const hashedPassword = await securePassword(newPassword);
    const sendEmail = await sendEmailServices(email, newPassword);
    const updatePass = await services.updatePassword(email, hashedPassword)
    return res.status(200).json(errorFunction(false, "Thank you! An email has been sent to " + email + " email id. Please check your inbox."))
  } catch (error) {
    console.log(error)
    next();
  }
};

exports.getUserDetailsById = async (req, res, next) => {
  try {
    const userDetail = await User.findOne({ _id: req.query.userId }).exec();
    if (!userDetail) {
      res.status(403);
      return res.json(errorFunction(true, "Unknown user"))
    } else {
      return res.status(200).json(errorFunction(false, "user details", userDetail));
    }
  } catch (error) {
    next(error)
  }
};

exports.getUserList = async (req, res, next) => {
  const userList = await User.find().exec();
  if (userList.length > 0) {
    return res.status(200).json(errorFunction(false, "user list", userList))
  } else {
    return res.json(errorFunction(true, "user list not found"));
  }
};


exports.activeUser = async (req, res, next) => {
  try {
    const activeUser = await User.find({ is_active: true }).sort({ "updatedAt": -1 });
    if (activeUser.length > 0) {
      return res.status(200).json(errorFunction(false, "Active User List", activeUser))
    } else {
      return res.json(errorFunction(true, "all users are offline"));
    }
  } catch (error) {
    next(error)
  }
};

exports.inactiveUser = async (req, res, next) => {
  try {
    const inactiveUser = await User.find({ is_active: false }).sort({ "updatedAt": -1 });
    if (inactiveUser.length > 0) {
      return res.status(200).json(errorFunction(false, "Inactive User List", inactiveUser))
    } else {
      return res.json(errorFunction(true, "no inactive users"));
    }
  } catch (error) {
    next(error)
  }
};






exports.updateProfile = async (req, res, next) => {
  const userDetail = await User.findOne({ _id: req.body.userId }).exec();
  if (userDetail) {
    let post = await User.updateOne({ _id: req.body.userId }, {
      $set: req.body
    })
    return res.status(200).json(errorFunction(false, "Profile details updated successfully"))
  } else {
    return res.json(errorFunction(true, "user not authenticated"));
  }
};

exports.changePassword = async (req, res, next) => {
  const userList = await User.find({ _id: req.body.userId });
  return res.status(200).json(errorFunction(false, "change password api in development"))
};


exports.contactUs = async (req, res, next) => {
  try {
    const newQuery = await Support.create({
      userId: req.body.userId,
      subject: req.body.subject,
      content: req.body.content,
    })
    if (newQuery) {
      res.status(201);
      return res.json(errorFunction(false, "Query Created"));
    } else {
      res.status(403);
      return res.json(errorFunction(true, "Error of create query"));
    }
  } catch (error) {
    res.status(400);
    console.log(error);
    return res.json(errorFunction(true, "Error Adding user"));
  }
};

exports.selectPlanDetails = async (req, res, next) => {
  const { userId, planType } = req.body
  try {
    const user = await User.findOne({ _id: userId });
    if (user) {
      const PlanDetails = await services.selectPlanDetails(userId, planType)
      if (PlanDetails === true) {
        return res.status(200).json(errorFunction(false, "Plan Added Successfully"));
      }
      return res.status(403).json(errorFunction(true, "something went wrong"));
    } else {
      res.status(403);
      return res.json(errorFunction(true, "user not authenticated"))
    }
  } catch (error) {
    next()
  }
};

exports.accountCurrency = async (req, res, next) => {
  const { userId, currency } = req.body
  try {
    const user = await User.findOne({ _id: userId });
    if (user) {
      const PlanDetails = await services.addAccountCurrency(userId, currency)
      if (PlanDetails === true) {
        return res.status(200).json(errorFunction(false, "Currency Added Successfully"));
      }
      return res.status(403).json(errorFunction(true, "something went wrong"));
    } else {
      res.status(403);
      return res.json(errorFunction(true, "user not authenticated"))
    }
  } catch (error) {
    next()
  }
};
exports.riskMode = async (req, res, next) => {
  const { userId, mode } = req.body
  try {
    const user = await User.findOne({ _id: userId });
    if (user) {
      const PlanDetails = await services.addriskMode(userId, mode)
      if (PlanDetails === true) {
        return res.status(200).json(errorFunction(false, "Risk Mode Added Successfully"));
      }
      return res.status(403).json(errorFunction(true, "something went wrong"));
    } else {
      res.status(403);
      return res.json(errorFunction(true, "user not authenticated"))
    }
  } catch (error) {
    next()
  }
};

exports.addAccountBalance = async (req, res, next) => {
  const { userId, amount } = req.body
  try {
    const user = await User.findOne({ _id: userId });
    if (user) {
      const planDetails = await services.addToBalance(userId, amount)
      if (planDetails === true) {
        return res.status(200).json(errorFunction(false, "Amount Added Successfully", user));
      }
      return res.status(403).json(errorFunction(true, "something went wrong"));
    } else {
      res.status(403);
      return res.json(errorFunction(true, "user not authenticated"))
    }
  } catch (error) {
    next()
  }
};
