const moment = require('moment');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
var request = require('request');
const finnhub = require('finnhub');

const Admin = require('../model/admin');
const Company = require('../model/company');
const Group = require('../model/group');
const errorFunction = require('../utils/errorFunction');
const group = require('../model/group');

exports.adminSignup = async (req, res, next) => {
  try {
    const post = new Admin();
    post.userName = req.body.userName;
    post.email = req.body.email;
    post.password = req.body.password;
    post.mobileNumber = req.body.mobileNumber;
    post.createdAt = moment().format("ll");
    post.updatedAt = moment().format("ll");
    await post.save();
    res.send(post)
  } catch (error) {
    next(error);
  }
};

exports.adminLogin = async (req, res, next) => {
  const { email, password } = req.body
  if (!email || !password || email == "" || password == "") {
    return res.status(422).json({ success: false, message: "Please add all fields" });
  }
  const admin = await Admin.findOne({ email });
  if (admin && password == admin.password) {

    const token = await jwt.sign({ email: email }, process.env.JWT_SECRET_KEY, {
      expiresIn: process.env.JWT_TOKEN_EXPIRED
    });

    res.status(200);
    return res.json(errorFunction(false, "Admin Login Successfully", { authToken: token }));
  } else {
    res.status(403);
    return res.json(errorFunction(true, "Invalid credentials"));
  }
};


exports.symbolsList = async (req, res, next) => {
  try {
    await request.get('https://fcsapi.com/api-v3/forex/list?type=forex&access_key=8MZxXMuVuatbI8d379zR7gggh', function (err, response, body) {
      var symbols = JSON.parse(body);
      if (!err && symbols.code == 200) {
        return res.status(200).json(errorFunction(false, "Symbols  list", symbols.response))
      }
    })
  } catch (error) {
    next(error);
  }
};

exports.forexExchanges = async (req, res, next) => {
  const api_key = finnhub.ApiClient.instance.authentications['api_key'];
  api_key.apiKey = "cegovaaad3i0qis38q40cegovaaad3i0qis38q4g"
  const finnhubClient = new finnhub.DefaultApi()

  finnhubClient.forexExchanges((error, data, response) => {
    return res.status(200).json(errorFunction(false, "Symbols  list", data))
  });
}

exports.forexSymbol = async (req, res, next) => {
  const api_key = finnhub.ApiClient.instance.authentications['api_key'];
  api_key.apiKey = "cegovaaad3i0qis38q40cegovaaad3i0qis38q4g"
  const finnhubClient = new finnhub.DefaultApi()

  finnhubClient.forexSymbols("OANDA", (error, data, response) => {
    return res.status(200).json(errorFunction(false, " forex Symbols  list", data))
  });
};



/*---------------------------- Ankur Work Start --------------------------------------------*/

exports.createCompany = async (req, res, next) => {
  try {
    if (Object.keys(req.body).length === 0) {
      res.status(401);
      return res.json(errorFunction(true, "Please send the data "));
    } else {
      const companyName = req.body.companyName;
      const result = await Company.findOne({name: companyName})
      if (result) {
        res.status(200);
        return res.json(errorFunction(false, "Company already exit "));
      } else {
        const post = new Company();
        post.name = companyName;
        post.logo = '';
        post.status = true;
        var saveStatus = await post.save();
      }
      if (saveStatus) {
        res.status(200);
        return res.json(errorFunction(false, "Company created Successfully"));
      } else {
        res.status(401);
        return res.json(errorFunction(true, "ISome Thing want wrong"));
      }
    }
  } catch (error) {
    console.log("this is error", error)
    next(error);
  }
};

exports.getCompanyList = async (req, res, next) => {
  try {
    const companyList = await Company.find();
    if (companyList) {
      return res.status(200).json(errorFunction(false, "Company list", companyList))
    } else {
      return res.json(errorFunction(true, "Company list not found"));
    }
  } catch (error) {
    console.log("this is error", error)
    next(error);
  }
}

exports.getCompanyDetails = async (req, res, next) => {
  try {
    const companyDetail = await Company.findOne({ _id: req.query.companyId }).exec();
    if (!companyDetail) {
      res.status(403);
      return res.json(errorFunction(true, "Unknown company"))
    } else {
      return res.status(200).json(errorFunction(false, "company details", companyDetail));
    }
  } catch (error) {
    next(error)
  }
}
/*---------------------------- Ankur Work End --------------------------------------------*/

exports.createGroup = async (req, res, next) => {
  try {
    const newGroup = await Group.create({
      name: req.body.name,
      symbols: req.body.symbols,
      // members: req.body.members,
      // commission: req.body.commission,
      // timeintervalfrom: req.body.timeintervalfrom,
      // timeintervalto: req.body.timeintervalto,
      // spread: req.body.spread,
      status: true,
      createdAt: Date.now(),
      updatedAt: ""
       
    })
    if (newGroup) {
      res.status(201);
      return res.json(errorFunction(false, "Group Created"));
    } else {
      res.status(403);
      return res.json(errorFunction(true, "Error Adding Group"));
    }
  } catch (error) {
    res.status(400);
    console.log(error);
    return res.json(errorFunction(true, "Error Adding Group"));
    next();
  }
};

exports.getGroupList = async (req, res, next) => {
  try {
    const groupList = await Group.find();
    if (groupList.length > 0) {
      return res.status(200).json(errorFunction(false, "Group list", groupList))
    } else {
      return res.json(errorFunction(true, "Group list not found"));
    }
  } catch (error) {
    next(error);
  }
};

exports.deleteGroup = async (req, res, next) => {
  try {
    const groupDetail = await Group.findOne({ _id: req.query.groupId }).exec();
    if (groupDetail) {
      const id = await Group.deleteOne({ _id: req.query.groupId }).exec();
      if (id) {
        res.status(403);
        return res.json(errorFunction(true, "group deleted sucessfully"))
      }
      else {
        res.status(500);
        return res.json(errorFunction(true, "error while deleting group"))
      }
    } else {
      return res.status(200).json(errorFunction(false, "Unknow Group", groupDetail));
    }
  } catch (error) {
    next(error)
  }
};

exports.getGroupDetails = async (req, res, next) => {
  try {
    const groupDetail = await Group.findOne({ _id: req.query.groupId }).exec();
    if (!groupDetail) {
      res.status(403);
      return res.json(errorFunction(true, "Unknown group"))
    } else {
      return res.status(200).json(errorFunction(false, "group details", groupDetail));
    }
  } catch (error) {
    next(error)
  }
};

exports.updateGroup = async (req, res, next) => {
  const groupDetail = await Group.findOne({ _id: req.body.groupId }).exec();
  if (groupDetail) {
    let post = await Group.updateOne({ _id: req.body.groupId }, {
      name: req.body.name,
      symbols: req.body.symbols,
      // members: req.body.members,
      // commission: req.body.commission,
      // timeintervalfrom: req.body.timeintervalfrom,
      // timeintervalto: req.body.timeintervalto,
      // spread: req.body.spread,
      status: true,
      updatedAt: Date.now()

    })
    return res.status(200).json(errorFunction(false, "Group details updated successfully"))
  }
  else {
    return res.status(500).json(errorFunction(true, "Unknown group not found"));
  }
};


exports.createSubAdmin = async (req, res, next) => {
  try {
    const newSubadmin = await Admin.create({
      userName: req.body.userName,
      email: req.body.email, 
      roles: req.body.roles,
      access: req.body.access,     
      password: req.body.password,
      mobileNumber: req.body.mobileNumber,
      createdAt: Date.now(),
      updatedAt: ""
    })
    if (newSubadmin) {
      res.status(201);
      return res.json(errorFunction(false, "Sub Admin Created"));
    } else {
      res.status(403);
      return res.json(errorFunction(true, "Error Adding Sub Admin"));
    }
  } catch (error) {
    res.status(400);
    console.log(error);
    return res.json(errorFunction(true, "Error Adding Sub Admin"));
    next();
  }
};

exports.subAdminList = async (req, res, next) => {
  try {
    const adminList = await Admin.find();
    if (adminList.length > 0) {
      return res.status(200).json(errorFunction(false, "SubAdmin list", adminList))
    } else {
      return res.json(errorFunction(true, "SubAdmin list not found"));
    }
  } catch (error) {
    next(error);
  }
};

exports.deleteSubAdmin = async (req, res, next) => {
  try {
    const subAdminDetail = await Admin.findOne({ _id: req.query.subAdminId }).exec();
    if (subAdminDetail) {
      const id = await Admin.deleteOne({ _id: req.query.subAdminId }).exec();
      if (id) {
        res.status(403);
        return res.json(errorFunction(true, "SubAdmin deleted sucessfully"))
      }
      else {
        res.status(500);
        return res.json(errorFunction(true, "error while deleting SubAdmin"))
      }
    } else {
      return res.status(200).json(errorFunction(false, "Unknow SubAdmin", subAdminDetail));
    }
  } catch (error) {
    next(error)
  }
};

exports.subAdminDetails = async (req, res, next) => {
  try {
    const subAdminDetails = await Admin.findOne({ _id: req.query.subAdminId }).exec();
    if (!subAdminDetails) {
      res.status(403);
      return res.json(errorFunction(true, "Unknown Sub Admin"))
    } else {
      return res.status(200).json(errorFunction(false, "Sub Admin details", subAdminDetails));
    }
  } catch (error) {
    next(error)
  }
};

exports.updateSubAdmin = async (req, res, next) => {
  const subAdminDetail = await Admin.findOne({ _id: req.body.subAdminId }).exec();
  if (subAdminDetail) {
    let post = await Admin.updateOne({ _id: req.body.subAdminId }, {
      userName: req.body.userName,
      email: req.body.email, 
      roles: req.body.roles,
      access: req.body.access,     
      password: req.body.password,
      mobileNumber: req.body.mobileNumber,
      updatedAt: Date.now,
    })
    return res.status(200).json(errorFunction(false, "Group details updated successfully"))
  }
  else {
    return res.status(500).json(errorFunction(true, "Unknown group not found"));
  }
};
