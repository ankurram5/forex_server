const userSchema = require('../model/user');
var generator = require('generate-password');

exports.userDetails = async (email) => {
  try {
    let user = await userSchema.findOne({ email: email }).exec();
    return user;
  } catch (error) {
    return false;
  }
}

exports.generatePassword = async () => {
  var password = generator.generate({
    length: 8,
    numbers: true,
    uppercase: false
  });
  return password
}

exports.updatePassword = async (email, hashedPassword) => {
  try {
    const output = await userSchema.updateOne({email: email}, { $set: {password: hashedPassword} });
    return output
  } catch (error) {
    return false;
  }
};

exports.selectPlanDetails = async (userId, planType) => {
  try {
    const output = await userSchema.updateOne({_id: userId}, {
      $set: {
        planType: planType, updatedAt: new Date().getTime()
      } 
    });
    return true
  } catch (error) {
    return false;
  }
};

exports.addAccountCurrency = async (userId, currency) => {
  try {
    const output = await userSchema.updateOne({_id: userId}, {
      $set: {
        accountCurrency: currency, updatedAt: new Date().getTime()
      }
    });
    return true
  } catch (error) {
    return false;
  }
};
exports.addriskMode = async (userId, mode) => {
  try {
    const output = await userSchema.updateOne({_id: userId}, {
      $set: {
        riskMode: mode, updatedAt: new Date().getTime() 
      } 
    });
    return true
  } catch (error) {
    return false;
  }
};

exports.addToBalance = async (userId, amount) => {
  try {
    const output = await userSchema.updateOne({_id: userId}, {
      $set: {
        accountBalance: amount, updatedAt : new Date().getTime()
      }
    });
    return true
  } catch (error) {
    return false;
  }
};