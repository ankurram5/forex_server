const express = require('express');
const bodyParser = require("body-parser");
let responseTime = require('response-time');
const logger = require('morgan');
const path = require('path');
const cors = require('cors');

require("dotenv").config();
require('./config/connection');

let routes = require('./routes/commonRoutes');
let managerRouter = require('./routes/managerRoutes');

const app = express();
const port = process.env.PORT;

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin");
  res.header("Access-Control-Allow-Methods", "*");
  next();
});

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }));

/* Middleware to log time for each api call */ 
app.use(responseTime());

app.use(express.static('./public'));
app.set('views', path.join(__dirname, 'views'));

app.get('/', (req, res) => {
  res.send('Hi!, welcome to forex servers')
})

app.use('/api/v1', routes);
app.use('/api/v1/manager', managerRouter)

app.listen(port, () => {
  console.log(`Server Started at ${port}`)
})