const express = require('express');
const session = require('express-session');

const router = express.Router()

const userValidation = require('../utils/user.validator');

const commonController = require('../controllers/commonController');
const userController = require('../controllers/userController');

router.use(session({
    secret: 'somerandomforex',
    resave: false,
    saveUninitialized: false,
    cookie: { expires: 6000000 }
}));

router.post('/signup', userValidation.signup, userController.signupUser);
router.post('/login', userValidation.login, userController.loginUser);
router.post('/forgotPassword', userValidation.forgotPassword,userController.forgotPassword);
router.put('/changePassword', userController.changePassword);
router.get('/userDetailsById', userValidation.userDetailsById,userController.getUserDetailsById);
router.get('/userList', userController.getUserList);
router.get('/getActiveUser', userController.activeUser);
router.get('/getInactiveUser', userController.inactiveUser);

router.put('/updateProfile',userValidation.updateProfile, userController.updateProfile);
router.post('/contact', userValidation.contact, userController.contactUs);

router.put('/selectPlan', userController.selectPlanDetails);
router.put('/addToCurrencyType', userController.accountCurrency);
router.put('/addRiskMode', userController.riskMode);
router.put('/addToBalance', userController.addAccountBalance);


module.exports = router;
