var express = require('express');
var router = express.Router();

const managerValidation = require('../utils/manager.validator');

const adminController = require('../controllers/adminController');
const auth = require('../middleware/auth');

router.post('/adminSignup',managerValidation.signUp, adminController.adminSignup);
router.post('/adminLogin', managerValidation.login,  adminController.adminLogin);

router.post('/createCompany', auth, managerValidation.createCompany,adminController.createCompany);
router.get('/getCompanyList', auth, adminController.getCompanyList);
router.get('/getCompanyDetails', auth, adminController.getCompanyDetails);


router.get('/forexExchanges', adminController.forexExchanges);
router.get('/getSymbolsList', adminController.symbolsList);
router.get('/forexSymbol', adminController.forexSymbol);

router.post('/createGroup',  adminController.createGroup );
router.get('/groupList',  adminController.getGroupList);
router.get('/groupDetails',  adminController.getGroupDetails);
router.post('/updateGroup', adminController.updateGroup );
router.delete('/deleteGroup', adminController.deleteGroup );


router.post('/createSubAdmin', adminController.createSubAdmin );
router.get('/subAdminList', adminController.subAdminList );
router.get('/subAdminDetails', adminController.subAdminDetails );
router.post('/updateSubAdmin', adminController.updateSubAdmin );
router.delete('/deleteSubAdmin', adminController.deleteSubAdmin );


module.exports = router;